<!DOCTYPE html>
<html>
	<head>
		<title>Shopping Cart</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/custom.css"/>		
	</head>
 
	<body>
		
		<nav class="navbar">
			<div class="container">
				<a class="navbar-brand" href="#">Welcome to Bookstore</a>
				<div class="navbar-right">
					<div class="container minicart">
						<label style="color: white">
							@if (empty($shoppingCart))
									{{0}}
								@else
									{{$shoppingCart->getNumberOfBooks()}}     
							@endif		                 
						</label>
					</div>
				</div>
			</div>
		</nav>
		
		<div class="container-fluid breadcrumbBox text-center">
			<form action="/welcome_basket" method="GET">
				<ol class="breadcrumb">				
					<li><input type="text" name="parameter" placeholder="Insert the parameter and ENTER"></li>
				</ol>
			</form>
		</div>
		
		<div class="container text-center">

			<div class="col-md-4 col-sm-12">
				<div class="bigcart"></div>
				<h1>Your shopping cart</h1>
				<p>
					This is a book shopping cart processor,
					made to load from a csv file. Reads through a
					csv and returns the final price along with the list of books.<br />
					@if (empty($shoppingCart))
							
						@else
						 Now you can see the <a href="{{action('ShoppingCartController@aggregated')}}">Aggregates Books.</a>  
					@endif	
					
				</p>
			</div>
			
			<div class="col-md-7 col-sm-12 text-left">
				<table class="table">
					<thead>
						<tr>
							<th scope="col" class="header_table">PRICE</th>
							
							@if (!empty($shoppingCart))
								@if (!empty($adicionalParameter))
									@if( strcmp($adicionalParameter, "-displayauthors") != 0)
									@endif

								@elseif (empty($shoppingCart->getBookQuantityArray()))  
									<th scope="col" class="header_table">TYPE</th>
								@endif
							@endif

							@if (empty($adicionalParameter))
								<th scope="col" class="header_table">ISBN</th>
							@elseif  (strcasecmp($adicionalParameter, "-displayauthors") != 0)
								<th scope="col" class="header_table">ISBN</th>
							@endif
							<th scope="col" class="header_table">TITLE</th>
							<th scope="col" class="header_table">AUTHORS</th>
						</tr>
					</thead>
					<tbody>
                        @if (empty($shoppingCart))
                                <p style="color: orange; font-weight: bold;">The basket is empty or invalid parameters.</p>
                            @else
                                @foreach ($shoppingCart->getBooks() as $book)
                                    <tr class="row_table">
                                        <td class="item_table" id="price">
											@if (empty($adicionalParameter))
												€  {{ $book->getPrice() }} 
											@endif
											@if (!empty($shoppingCart->getBookQuantityArray()))
												({{ $shoppingCart->getBookQuantity($book) }})
											@elseif (!empty($adicionalParameter))
												@if( strcasecmp($adicionalParameter, "-displayauthors") == 0)
													€  {{ number_format( $book->getPrice()  /(1 - $book->getValueOfDiscount()), 2, '.', '' )   }} / {{ $book->getPrice() }} 
												@endif
											@endif	

										</td>
										@if (empty($shoppingCart->getBookQuantityArray()))
											@if (!empty($adicionalParameter))
												@if( strcasecmp($adicionalParameter, "-displayauthors") != 0)
												@endif
											@elseif (empty($shoppingCart->getBookQuantityArray())) 
                                        		<td class="item_table">{{ class_basename ($book) }}</td>
											@endif	
										@endif	

										@if (empty($adicionalParameter))
											<td class="item_table">{{ $book->getIsbn() }}</td>
										@elseif (strcasecmp($adicionalParameter, "-displayauthors") != 0)
										    <td class="item_table">{{ $book->getIsbn() }}</td>
										@endif
											<td class="item_table">{{ $book->getTitle() }}</td>
											<td class="item_table">
										@foreach ($book->getAuthors() as $author)
												{{$author}}
										@endforeach
										
                                    </tr>
                                @endforeach
                            @endif    
					</tbody>
				</table>

				<ul>
					<li class="row totals">
						<span class="itemName">Total:</span>
						@if (empty($shoppingCart))
							<span class="price">€ 0</span> 
							@else
							<span class="price">€ {{$shoppingCart->getTotalPayable()}}</span>   
						@endif
						   
						<span class="order"> <a href="{{action('ShoppingCartController@load')}}" class="text-center">LOAD BASKET</a></span>
					</li>
				</ul>
			</div>

		</div>

		<!-- The popover content -->

		<div id="popover" style="display: none">
			<a href="#"><span class="glyphicon glyphicon-pencil"></span></a>
			<a href="#"><span class="glyphicon glyphicon-remove"></span></a>
		</div>
		
		<!-- JavaScript includes -->

		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/customjs.js') }}"></script>

	</body>
</html>