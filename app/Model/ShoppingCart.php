<?php

namespace app\Model;

class ShoppingCart
{
    
    var $books = array();
    var $bookQuantity = array();
    var $totalPayable = 0;

    public function addBook($book) 
    {
        $newPrice = $book->getPriceUpdated();
        $this->books[] = $book;
        $this->totalPayable += $book->getPrice();
    }

    public function getBooks()
    {
        return $this->books;
    }
    
    public function getNumberOfBooks()
    {
        return count($this->books);
    }
    
    public function getTotalPayable()
    {
        return number_format($this->totalPayable, 2, '.', '');
    }

    public function setBookQuantity($book, $quantity)
    {
        $this->bookQuantity["{$book->getIsbn()}"] = $quantity;
    }

    public function getBookQuantity( $book)
    {  
        return $this->bookQuantity[$book->getIsbn()];
    }

    public function getBookQuantityArray()
    {
        return $this->bookQuantity;
    }

    public function update($book, $quantity)
    {
        $this->addBook($book);
        $this->setBookQuantity($book, $quantity);
    }

}
