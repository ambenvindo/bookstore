<?php

namespace app\Model;

use App\Model\Book;
use app\Util\Generic;

class UsedBook extends Book
{
    private $discount = 0.25;

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getValueOfDiscount()
    {
        return number_format($this->discount, 2, '.', '');
    }
}