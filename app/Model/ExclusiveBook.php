<?php

namespace app\Model;

use App\Model\Book;

class ExclusiveBook extends Book
{
    private $discount = 0;

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getValueOfDiscount()
    {
        return number_format($this->discount, 2, '.', '');
    }
}