<?php

namespace app\Model;

use App\Discount;

abstract class Book
{
    var $title;
	var $authors = array();
	var $isbn;
    var $price = 0;

    public function __construct($title, $isbn, $price, $authors)
    {
        $this->title = $title;
        $this->isbn = $isbn;
        $this->price = $price;
        $this->authors = explode("|", $authors); ;
    }

    public function setTitle( $title )
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setAuthors( $authors )
    {
        $this->authors = $authors;
    }

    public function getAuthors()
    {
        return $this->authors;
    }

    public function setIsbn( $isbn )
    {
        $this->isbn = $isbn;
    }

    public function getIsbn()
    {
        return $this->isbn;
    }

    public function setPrice( $price )
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return number_format($this->price, 2, '.', '');
    }

    public function getPriceUpdated()
    {
        $newPrice = $this->price - ( $this->price * $this->getValueOfDiscount() );         
        return number_format( $newPrice, 2, '.', '' );
    }

    public abstract function getValueOfDiscount();
    
}