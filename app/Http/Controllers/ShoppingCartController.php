<?php

namespace App\Http\Controllers;

use App\Util\Basket;
use App\Model\ShoppingCart;
use Illuminate\Http\Request;

class ShoppingCartController extends Controller
{

    protected $basket;

    public function __construct()
    {
        $this->basket = new Basket();
        # code...
    }

    public function load()
    {
        $shoppingCart = $this->basket->getAllItens();
        return view('welcome')->with('shoppingCart', $shoppingCart);
    }

    public function aggregated()
    {   
        $shoppingCart = $this->basket->getAllItens();
        $shoppingCartAggregated = $this->basket->getAgregatedBooks($shoppingCart);
        return view('welcome')->with('shoppingCart', $shoppingCartAggregated);
    }

    public function seachBasket(Request $request)
    {
        $parameter = $request->input('parameter');
        $shoppingCart = $this->basket->getByParameter($parameter);
        if (count($shoppingCart->getBooks()) == 0) return view('welcome');
        $adicionalParameter = $this->basket->getAdicionalParameter();
        return view('welcome', compact('shoppingCart', 'adicionalParameter'));
    }

    public function get()
    {
        $shoppingCart = $this->basket->getAllItens();
        $this->basket = ['basket' => $shoppingCart];
        return response()->json($this->basket);
    }

    public function addItem(Request $request)
    {
        $book = json_decode($request);

        // $shoppingCart->addBook($book);
        // $shoppingCart = array_values($shoppingCart); 
        // $shoppingCart = $this->basket->getAllItens();
        // $this->basket = ['basket' => $shoppingCart];
        // return response()->json($this->basket);
        return response()->json($this->book);
    }


}
