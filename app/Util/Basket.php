<?php

namespace App\Util;

use App\Model\ShoppingCart;
use App\Model\UsedBook;
use App\Model\NewBook;
use App\Model\ExclusiveBook;

class Basket
{

    protected $adicionalParameter;

    public function getAllItens()
    {
        $shoppingCart = new ShoppingCart();
        $book;

        if (($handle = fopen(Generic::BASKET_FILE, "r")) !== FALSE)  
        {
            $data = fgetcsv($handle, 1000, ",");  // Reading the header first

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
            {     
                switch ($data[0]) 
                {
                    case 'UsedBook':
                        $book = new UsedBook($data[1], $data[2], $data[3], $data[4]);
                    break;

                    case 'NewBook':
                        $book = new NewBook($data[1], $data[2], $data[3], $data[4]);
                    break;

                    case 'ExclusiveBook':
                        $book = new ExclusiveBook($data[1], $data[2], $data[3], $data[4]);
                    break;
                }
                $book->setPrice($book->getPriceUpdated()); 
                $shoppingCart->addBook($book);
            }
            fclose($handle);
        }

        return $shoppingCart;
    }



    public function getByParameter($parameter)
    {
        $separator = explode(" ", $parameter);
        $fileParameter = current($separator); 
        $this->adicionalParameter = next($separator);        
        if (!empty($this->adicionalParameter) && strcmp($this->adicionalParameter, "-displayauthors") != 0)
        {
            return new ShoppingCart();
        }   
        $fileName = 'C:\\Users\\wawingui.antonio\\Documents\\Benvindo Amaro\\Projecto\\BookStore\\' . $fileParameter . ".csv" ;
        return file_exists($fileName ) ? $this->getAllItens() : new ShoppingCart();;       
    }



    public function getAdicionalParameter()
    {
        return $this->adicionalParameter;
    }



    public function getAgregatedBooks(ShoppingCart $shoppingCart) 
    {
        $shoppingCartWithAgregatedBooks = new ShoppingCart();
        $arrayBooks = $shoppingCart->getBooks();

        for ( $count = 0; $count < count($arrayBooks) - 1; $count++)         
        {
            $currentBook = $arrayBooks[$count];          
            $aggregate = $currentBook->getPrice();
            $quantity = 1;

            for ( $countNext = $count + 1; $countNext < count($arrayBooks); $countNext++)         
            {
                $nextBook = $arrayBooks[$countNext];

                if (strcasecmp($currentBook->getIsbn(), $nextBook->getIsbn()) == 0)
                {
                    $aggregate += $nextBook->getPrice();
                    $currentBook->setPrice($aggregate);
                    unset($arrayBooks[$countNext]);
                    $quantity++;
                }    
            }
            if ($quantity > Generic::MINIMUM_QUANTITY_IN_BASKET)
            {
                $shoppingCartWithAgregatedBooks->update($currentBook, $quantity);
                $arrayBooks = array_values($arrayBooks);   // updating the array books
            } 
        }
        return $shoppingCartWithAgregatedBooks;
    }    

}

