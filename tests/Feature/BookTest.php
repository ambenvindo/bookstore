<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Model\Book;
use App\Model\UsedBook;
use App\Model\NewBook;
use App\Model\ExclusiveBook;

class BookstoreTest extends TestCase
{
    
    public function testBookInstance()
	{
        $title = "Java How to Program, Early Objects";
        $isbn = "978-0134743356";
        $price = 144.83;
        $authors = "Paul Deitel|Harvey Deitel";       
        $bookUsed = new UsedBook($title, $isbn, $price, $authors);
        $bookExclusive = new ExclusiveBook($title, $isbn, $price, $authors);

        $this->assertEquals($bookExclusive->getValueOfDiscount(), 0);
        $this->assertEquals($bookUsed->getValueOfDiscount(), 0.25);
        $this->assertFalse(strcmp($bookUsed->getTitle(), 'Java How to Program, Early Objects ') == 0);
    }
    
    
}
